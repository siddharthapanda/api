package com.Infostretch.JsonPathLibrabry;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.testng.Reporter;
import org.testng.annotations.Test;
import com.jayway.jsonpath.Criteria;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.Filter;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;

public class assignment {
	static FileReader fileReader;
	static String file;
	static Object json;
	static DocumentContext jsonContext;

	static {
		file = System.getProperty("user.dir") + "\\src\\test\\resources\\sample-data.json";
		try {
			fileReader = new FileReader(file);
			json = new JSONParser(0).parse(fileReader);
			jsonContext = JsonPath.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	@Test
	public static void allUserNames() throws ParseException, IOException {
		int iTemp = 1;
		Reporter.log("------------Please Find All User Names As Below-----------------");
		for (String user :JsonUtility.readJson(file, "$.[*].name")) {
			Reporter.log("All user " + iTemp + " is " +user);
			assertTrue(user.length()>1);
		}
	}

	@Test
	public static void allFemaleUsers() throws FileNotFoundException, ParseException, IOException {
		Filter expensiveFilter = Filter.filter(Criteria.where("gender").eq("female"));
		List<Map<String, Object>> expensive = JsonPath.parse(json).read("$[*][?]", expensiveFilter);
		Reporter.log("-----------------Please Find All Female Users As Below------------------------");
		for (int iTemp = 0; iTemp < expensive.size(); iTemp++) {
			String femaleUser = (String) expensive.get(iTemp).get("name");
			Reporter.log("Female user " + iTemp + " is " + femaleUser);
		}
	}

	@Test
	public static void firstTwoUsers() throws FileNotFoundException, ParseException, IOException {
		Reporter.log("-----------------Please Find First Two Users As Below------------------------");
		int iTemp = 1;
		for (String user : JsonUtility.readJson(file,"[:2].name")) {
			Reporter.log("User Number " + iTemp + " is =" + user);

		}
	}

	@Test
	public static void lastTwoUsers() throws FileNotFoundException, ParseException, IOException {
		Reporter.log("-----------------Please Find Last Two Users As Below------------------------");
		int iTemp = 1;
		for (String user :JsonUtility.readJson(file,"[-2:].name")) {
			Reporter.log("User Number " + iTemp + " is =" + user);

		}
	}

	@Test
	public static void totalNumberOfFriendsForFirstUser()
			throws FileNotFoundException, ParseException, IOException {
		String firstUserFriends = "$.[0].friends";
		List<Map<String, Object>> userFriendsList = JsonPath.parse(json).read(firstUserFriends);
		System.out.println("Total Number of friends for first user =" + userFriendsList.size());
		Reporter.log("Total Number of friends for first user =" + userFriendsList.size());
		Reporter.log("-----------------Please Find List of Friends of First User As Below------------------------");
		for (int iTemp = 0; iTemp < userFriendsList.size(); iTemp++) {
			String firstUserFriend = (String) userFriendsList.get(iTemp).get("name");
			int userNum = iTemp + 1;
			System.out.println("Friend of First User " + userNum + " is " + firstUserFriend);
			Reporter.log("Friend of First User " + userNum + " is " + firstUserFriend);

		}
	}

	@Test
	public static void usersHavingAgeBetween24And36() throws FileNotFoundException, ParseException, IOException {
		Reporter.log(
				"-----------------Please Find Users having Age Between 24 and 36 As Below------------------------");
		int iTemp = 1;
		for (String user : JsonUtility.readJson(file,"$.[*][?(@.age>24 && @.age<36)].name")) {
			System.out.println(iTemp + " User having age between 24 and 36 is =" + user);
			Reporter.log("User " + iTemp + " is -" + user);
		}
	}

	@Test
	public static void balanceOfMaleUsers() throws FileNotFoundException, ParseException, IOException {
		Filter maleUsers = Filter.filter(Criteria.where("gender").eq("male"));
		List<Map<String, Object>> expensive = JsonPath.parse(json).read("$[*][?]", maleUsers);
		Reporter.log("-----------------Please Find Balance of Male Users As Below------------------------");
		for (int iTemp = 0; iTemp < expensive.size(); iTemp++) {
			String userName = (String) expensive.get(iTemp).get("name");
			String balance = (String) expensive.get(iTemp).get("balance");
			System.out.println(userName + " has balance of " + balance);
			Reporter.log(userName + " has balance of " + balance);
		}
	}

	@Test
	public static void usersLivesInCalifornia() throws FileNotFoundException, ParseException, IOException {
		Filter userLiveInCalifornia = Filter.filter(Criteria.where("address").regex(Pattern.compile(".*California.*")));
		List<Map<String, Object>> expensive = JsonPath.parse(json).read("$[*][?]", userLiveInCalifornia);
		Reporter.log("-----------------Please Find Users Who Lives in California As Below------------------------");
		for (int iTemp = 0; iTemp < expensive.size(); iTemp++) {
			String userName = (String) expensive.get(iTemp).get("name");
			System.out.println("User which lives in California " + iTemp + " is " + userName);
			Reporter.log(iTemp + 1 + " User which lives in California is =>" + userName);
		}
	}

}
