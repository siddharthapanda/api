package com.Infostretch.JsonPathLibrabry;

import java.io.FileReader;
import java.util.List;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.parser.JSONParser;


public class JsonUtility {
	static FileReader fileReader;
	static String file;
	static Object json;
	static DocumentContext jsonContext;

	public static List<String> readJson(String file, String condition) {
		try {
			fileReader = new FileReader(file);
			json = new JSONParser(0).parse(fileReader );
			jsonContext = JsonPath.parse(json);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return jsonContext.read(condition);

	}

}
